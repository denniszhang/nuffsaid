import React, { Component } from 'react'
import Message from './Message'
import Grid from '@material-ui/core/Grid'

const MessageType = (props) => {
    const { messages, priority, title, color } = props
    return (
        <div>
            <h2>{title} {priority}</h2>
            <h4>Count {messages.length}</h4>
            <Grid container spacing={16}>
                {
                    messages.map((message, index) => (
                        <Grid item xs={12} key={title+':'+index}>
                            <Message message={message} color={color} handleClear={props.handleClear} />
                        </Grid>
                    ))
                }
            </Grid>
        </div>
    )
}

export default MessageType
