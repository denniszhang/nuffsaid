import React, { useState } from 'react'
import styled from 'styled-jss'

const MessageArea = styled('div')({
    position: 'absolute',
    bottom: '10px',
    right: '10px',
    textTransform: 'capitalize'
})

const Wrapper = styled('div')(({ color }) => ({
    position: 'relative',
    background: color,
    height: '60px',
    width: '100%',
    borderRadius: '4px',
    padding: '10px',
    boxShadow: '1px 1px #CCC'
}))

const Message = (props) => {
    return (
        <Wrapper color={props.color}>
            {props.message.message}
            <MessageArea onClick={() => props.handleClear(props.message)}>
                Clear
            </MessageArea>
        </Wrapper>
    )
}

export default Message