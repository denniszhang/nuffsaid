import React, { useState, useEffect } from 'react'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import Api from '../api'
import MessageType from './MessageType'
import styled from 'styled-jss'
import Snackbar from '@material-ui/core/Snackbar'

const TopDiv = styled('div')({
  display: 'flex',
  justifyContent: 'center'
})

const NormalButton = styled(Button)({
  background: '#88FCA3',
  textTransform: 'capitalize',
  margin: '5px'
})

const Container = styled('div')({
  padding: '20px 100px'
})

let api = null

const MessageList = () => {
  const [messages, setMessages] = useState([])
  const [isApiStarted, setIsApiStarted] = useState(true)
  const [isSnackbarShow, setIsSnackbarShow] = useState(false)
  const [snackbarMessage, setSnackbarMessage] = useState('')

  const messageCallback = (message) => {
    (message.priority === 1) ? (setIsSnackbarShow(true), setSnackbarMessage(message.message)) : true
    setMessages(messages => [message, ...messages.slice(),])
  }

  const getMessages = (type) => {
    return messages.filter(message => message.priority === type)
  }

  const handleClearAllMessages = () => {
    setIsSnackbarShow(false)
    setSnackbarMessage('')
    setMessages([])
  }

  const handleClear = message => {
    (message.message === snackbarMessage) ? (setIsSnackbarShow(false), setSnackbarMessage('')) : true
    setMessages(messages => messages.filter(m => m !== message))
  }

  useEffect(() => {
    api = new Api({
      messageCallback: (message) => {
        messageCallback(message)
      }
    })
    api.start()
  }, [])

  const renderButton = () => {
    return (
      <TopDiv>
        <NormalButton
          variant="contained"
          onClick={() => {
            if (isApiStarted) {
              api.stop()
            } else {
              api.start()
            }
            setIsApiStarted(!isApiStarted)
          }}
        >
          {isApiStarted ? 'Stop' : 'Start'}
        </NormalButton>
        <NormalButton
          variant="contained"
          onClick={handleClearAllMessages}>
          Clear
        </NormalButton>
      </TopDiv>
    )
  }

  const renderContent = () => {
    return (
      <Container>
        <Grid container spacing={40}>
          <Grid item xs={4}>
            <MessageType priority='1' color='#F56236' title='Error Type' messages={getMessages(1)} handleClear={handleClear} />
          </Grid>
          <Grid item xs={4}>
            <MessageType priority='2' color='#FCE788' title='Warning Type' messages={getMessages(2)} handleClear={handleClear} />
          </Grid>
          <Grid item xs={4}>
            <MessageType priority='3' color='#88FCA3' title='Info Type' messages={getMessages(3)} handleClear={handleClear} />
          </Grid>
        </Grid>
        <Snackbar
          open={isSnackbarShow}
          autoHideDuration={2000}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          message={snackbarMessage}
          onClose={() => {
            setIsSnackbarShow(false)
            setSnackbarMessage('')
          }} />
      </Container>
    )
  }

  return (
    <div>
      {renderButton()}
      {renderContent()}
    </div>
  )
}

export default MessageList
