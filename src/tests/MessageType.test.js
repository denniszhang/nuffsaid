import React from 'react'
import ReactDOM from 'react-dom'
import renderer from 'react-test-renderer'
import { mount } from 'enzyme'

import MessageType from '../components/MessageType'


describe('Test MessageType Component', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<MessageType priority='2' color='red' title='Type' messages={[]} handleClear={()=>{}} />, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});
