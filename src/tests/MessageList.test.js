import React from 'react'
import ReactDOM from 'react-dom'
import renderer from 'react-test-renderer'
import { mount } from 'enzyme'

import MessageList from '../components/MessageList'


describe('Test MessageList Component', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<MessageList />, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});
