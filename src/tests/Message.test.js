import React from 'react'
import ReactDOM from 'react-dom'
import renderer from 'react-test-renderer'
import { mount } from 'enzyme'

import Message from '../components/Message'


describe('Test Message Component', () => {
    it('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Message message={{message:'abc', priority:1}} color='red' handleClear={() => {}} />, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});
